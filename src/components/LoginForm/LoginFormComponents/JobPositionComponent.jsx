import {
  Button,
  Card,
  Checkbox,
  Col,
  Form,
  Radio,
  Row,
  Space,
  Spin,
  Typography,
} from "antd";
import React, { useContext, useState } from "react";
import { ArrowLeftOutlined } from "@ant-design/icons";
import "../Styles/Styles.css";
import { useNavigate } from "react-router-dom";
import CommonStyle from "./CommonStyle";
import MyContext from "./myContext/MyContext";

const tailFormItemLayout = {
  wrapperCol: {
    xs: {
      span: 24,
    },
    sm: {
      span: 16,
    },
  },
};

const formTailLayout = {
  labelCol: {
    span: 4,
  },
  wrapperCol: {
    span: 8,
  },
};

function JobPositionComponent(props) {
  const [form] = Form.useForm();
  const navigate = useNavigate();
  const [loading, setLoading] = useState(false);

  const { id, setId } = useContext(MyContext);

  const formItemLayout = {
    labelCol: {
      xs: {
        span: 24,
      },
      sm: {
        span: 8,
      },
    },
    wrapperCol: {
      xs: {
        span: 24,
      },
      sm: {
        span: 16,
      },
    },
  };

  const onFinish = (values) => {
    async function fetchData() {
      setLoading(true);
      await fetch(
        `https://dev.octilus.in/api/update.php?id=${id}&job_title=${values?.job_title}`,
        {
          method: "POST",
          mode: "cors",
        }
      )
        .then((response) => response.json())
        .then((data) => {
          console.log(data);
          setLoading(false);
          navigate("/completed");
        })
        .catch((error) => {
          setLoading(false);
          console.log(error);
        });
    }
    fetchData();
  };

  return (
    <>
      {loading ? (
        <Spin
          size="large"
          style={{
            display: "flex",
            alignContent: "center",
            justifyContent: "center",
            paddingTop: "45vh",
          }}
        />
      ) : (
        <Row>
          <Col span={12}>
            <CommonStyle />
          </Col>
          <Col span={12}>
            <div className="center-wrapper">
              <Card style={{ width: "25vw" }}>
                <Button
                  type="text"
                  icon={<ArrowLeftOutlined />}
                  onClick={() => navigate("/basicinfo")}
                >
                  PREVIOUS STEP
                </Button>
                <Typography.Title level={3}>
                  Select Job Position
                </Typography.Title>
                <hr></hr>
                <br></br>
                <Form
                  {...formItemLayout}
                  form={form}
                  name="register"
                  onFinish={onFinish}
                  style={{
                    maxWidth: 600,
                  }}
                  scrollToFirstError
                  initialValues={{
                    job_title: "Frontend Developer",
                    agreement: false,
                  }}
                >
                  <Form.Item
                    name="job_title"
                    rules={[
                      { required: true, message: "Please pick an item!" },
                    ]}
                  >
                    <Radio.Group ini>
                      <Space direction="vertical">
                        <Radio value={"Frontend Developer"}>
                          Frontend Developer
                        </Radio>
                        <Radio value={"WordPress Developer"}>
                          WordPress Developer
                        </Radio>
                        <Radio value={"UI/UX Designer"}>UI/UX Designer</Radio>
                        <Radio value={"Support Engineer"}>
                          Support Engineer
                        </Radio>
                        <Radio value={"Graphic Designer"}>
                          Graphic Designer
                        </Radio>
                      </Space>
                    </Radio.Group>
                  </Form.Item>

                  <Form.Item
                    name="agreement"
                    valuePropName="checked"
                    rules={[
                      {
                        validator: (_, value) =>
                          value
                            ? Promise.resolve()
                            : Promise.reject(
                                new Error("Should accept agreement")
                              ),
                      },
                    ]}
                    {...tailFormItemLayout}
                  >
                    <Checkbox>
                      <h6>
                        I accept the <a href="">Term of Conditions</a> and{" "}
                        <a href="">Privacy Policy</a>
                      </h6>
                    </Checkbox>
                  </Form.Item>

                  <Form.Item {...formTailLayout}>
                    <Button
                      style={{
                        color: "white",
                        background: "hsl(162.43deg 98.91% 35.88%)",
                      }}
                      type="primary"
                      htmlType="submit"
                    >
                      Submit
                    </Button>
                  </Form.Item>
                </Form>
              </Card>
            </div>
          </Col>
        </Row>
      )}
    </>
  );
}

export default JobPositionComponent;
