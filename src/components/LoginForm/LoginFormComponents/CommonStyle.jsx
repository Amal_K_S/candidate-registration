import {
  FacebookFilled,
  InstagramFilled,
  LinkedinFilled,
  TwitterCircleFilled,
} from "@ant-design/icons";
import { Col, Row, Typography } from "antd";
import React from "react";
import "../Styles/Styles.css";

function CommonStyle() {
  const { Title } = Typography;

  return (
    <>
      <div
        className="container"
        style={{ display: "flex", flexDirection: "column", height: "100vh" }}
      >
        <div
          style={{
            flex: "90%",
            // display: "flex",
            // justifyContent: "center",
            // alignItems: "center",
          }}
        >
          <Typography.Title
            level={1}
            style={{ color: "white", paddingTop: "25vh", paddingLeft: "8vw" }}
          >
            Let's Join with us !
          </Typography.Title>
          <Typography.Title
            level={4}
            style={{ color: "white", paddingLeft: "8vw" }}
          >
            Loreum lpsum is simply dummy text of the printing and type setting
            industry.
          </Typography.Title>
        </div>
        <div style={{ flex: "10%", paddingLeft: "2vw" }}>
          <Row>
            <Col span={16}>
              <Typography.Title level={4} style={{ color: "white" }}>
                Copyright © 2022 Octilus Technologies
              </Typography.Title>
            </Col>
            <Col span={8}>
              <Typography.Title level={4} style={{ color: "white" }}>
                <FacebookFilled />
                &nbsp;
                <TwitterCircleFilled /> &nbsp;
                <LinkedinFilled />
                &nbsp;
                <InstagramFilled />
              </Typography.Title>
            </Col>
          </Row>
        </div>
      </div>
      {/* <div className="container"> */}
      {/* <Row gutter={[10, 10]}>
          <Col span={24}>
            <Title className="textAlign center">Let's Join with us !</Title>
          </Col>
          <Col span={24}>
            <Title level={4} className="textAlign center">
              Some Text ....
            </Title>
          </Col>
        </Row> */}
      {/* <div className="division">
          <div className="eighty">
            <Title>Let's join with us !</Title>
            <br></br>
            <Title level={4}>Something is here ........</Title>
          </div>
          <div className="twenty">
            <Title level={4}>Copyright © 2022 Octilus Technologies</Title>
          </div>
        </div>
      </div> */}
    </>
  );
}

export default CommonStyle;
