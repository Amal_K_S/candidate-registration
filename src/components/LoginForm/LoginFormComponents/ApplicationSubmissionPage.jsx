import { Col, Row } from "antd";
import React from "react";
import completed from "../../../assets/page3.jpeg";

function ApplicationSubmissionPage() {
  return (
    <>
      {/* <Row>
        <Col>
          <img
            src={completed}
            alt="My Image"
            style={{ height: "100%", width: "100%" }}
          ></img>
        </Col>
      </Row> */}
      <div className="image-container">
        <img className="my-image" src={completed} alt="My Image" />
      </div>
    </>
  );
}

export default ApplicationSubmissionPage;
