import {
  Button,
  Card,
  Col,
  Form,
  Input,
  message,
  Radio,
  Row,
  Select,
  Spin,
  Typography,
} from "antd";
import { useContext, useState } from "react";
import { useNavigate } from "react-router-dom";
import CommonStyle from "./CommonStyle";
import MyContext from "./myContext/MyContext";
import "../Styles/Styles.css";

const { Option } = Select;

const formItemLayout = {
  labelCol: {
    xs: {
      span: 24,
    },
    sm: {
      span: 8,
    },
  },
  wrapperCol: {
    xs: {
      span: 24,
    },
    sm: {
      span: 16,
    },
  },
};

const formTailLayout = {
  labelCol: {
    span: 4,
  },
  wrapperCol: {
    span: 8,
  },
};

const PersonalInfoComponent = (props) => {
  const navigate = useNavigate();
  const [form] = Form.useForm();
  const [loading, setLoading] = useState(false);

  const { id, setId } = useContext(MyContext);

  const onFinish = (values) => {
    setLoading(true);
    async function fetchData() {
      await fetch(
        `https://dev.octilus.in/api/create.php?name=${values?.username}&phone=${values?.phone}&email=${values?.email}&gender=${values?.gender}`,
        {
          method: "POST",
          mode: "cors",
        }
      )
        .then((response) => response.json())
        .then((data) => {
          console.log(data);
          setId(data?.id);
          // props?.setId(data?.id);
          setLoading(false);
          navigate("/jobposition");
        })
        .catch((error) => {
          setLoading(false);
          message.error("OOPS! Something Went Wrong ...");
        });
    }
    fetchData();
  };
  const prefixSelector = (
    <Form.Item name="prefix" noStyle>
      <Select
        style={{
          width: 70,
        }}
      >
        <Option value="+91">+91</Option>
      </Select>
    </Form.Item>
  );

  const size = {
    borderRadius: "0px",
    width: "20vw",
  };

  return (
    <>
      {loading ? (
        <Spin
          size="large"
          style={{
            display: "flex",
            alignContent: "center",
            justifyContent: "center",
            paddingTop: "45vh",
          }}
        />
      ) : (
        <Row>
          <Col span={12}>
            <CommonStyle />
          </Col>
          <Col span={12}>
            <div className="center-wrapper">
              <Card>
                <Typography.Title level={3}>
                  Personal Information
                </Typography.Title>
                <Form
                  {...formItemLayout}
                  form={form}
                  name="Next"
                  onFinish={onFinish}
                  style={{
                    maxWidth: 600,
                  }}
                  scrollToFirstError
                  initialValues={{
                    username: "",
                    email: "",
                    prefix: "+91",
                    phone: "",
                    gender: "Male",
                  }}
                >
                  <Form.Item
                    name="username"
                    rules={[
                      {
                        required: true,
                        message: "Please input your username!",
                      },
                    ]}
                  >
                    <Input placeholder="Name" style={size} />
                  </Form.Item>

                  <Form.Item
                    name={"email"}
                    rules={[
                      {
                        type: "email",
                        message: "The input is not valid E-mail!",
                      },
                      {
                        required: true,
                        message: "Please input your E-mail!",
                      },
                    ]}
                  >
                    <Input placeholder="Email Address" style={size} />
                  </Form.Item>

                  <Form.Item
                    name="phone"
                    rules={[
                      {
                        required: true,
                        message: "Please input your phone number!",
                      },
                      {
                        pattern:
                          /^(?:(?:(?:\+|0{0,2})91[\s-]?)|(?:\((?:\+|0{0,2})91\)(?:[\s-])?)|(?:0{0,2})?)?[6789]\d{9}$/,
                        message: "Please input a valid phone number!",
                      },
                    ]}
                  >
                    <Input
                      addonBefore={prefixSelector}
                      style={size}
                      placeholder="Phone"
                    />
                  </Form.Item>

                  <Form.Item
                    name="gender"
                    rules={[
                      { required: true, message: "Please pick an item!" },
                    ]}
                  >
                    <Radio.Group ini>
                      <Radio.Button value="Male">Male</Radio.Button>
                      <Radio.Button value="Female">Female</Radio.Button>
                    </Radio.Group>
                  </Form.Item>

                  <Form.Item {...formTailLayout}>
                    <Button
                      type="primary"
                      htmlType="submit"
                      style={{
                        color: "white",
                        background: "hsl(162.43deg 98.91% 35.88%)",
                      }}
                    >
                      Next
                    </Button>
                  </Form.Item>
                </Form>
              </Card>
            </div>
          </Col>
        </Row>
      )}
    </>
  );
};
export default PersonalInfoComponent;
