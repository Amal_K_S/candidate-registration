import { Card, Col, Row } from "antd";
import React, { useState } from "react";
import ApplicationSubmissionPage from "./ApplicationSubmissionPage";
import JobPositionComponent from "./JobPositionComponent";
import PersonalInfoComponent from "./PersonalInfoComponent";
import {
  BrowserRouter as Router,
  Routes,
  Route,
  Link,
  BrowserRouter,
} from "react-router-dom";
import MyContext from "./myContext/MyContext";

function LoginFormComponent() {
  const [id, setId] = useState(undefined);

  console.log("the setted id", id);

  return (
    <>
      <MyContext.Provider value={{ id, setId }}>
        <BrowserRouter>
          <Routes>
            <Route path="/" element={<PersonalInfoComponent />}></Route>
            <Route path="basicinfo" element={<PersonalInfoComponent />} />
            <Route
              path="jobposition"
              element={<JobPositionComponent id={id} />}
            />
            <Route path="completed" element={<ApplicationSubmissionPage />} />
          </Routes>
        </BrowserRouter>
      </MyContext.Provider>
    </>
  );
}

export default LoginFormComponent;
