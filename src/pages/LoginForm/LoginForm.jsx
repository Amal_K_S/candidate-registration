import React from "react";
import { Row, Col } from "antd";
import LoginFormComponent from "../../components/LoginForm/LoginFormComponents/LoginFormComponent";

function LoginForm() {
  return (
    <Row gutter={[10, 10]}>
      <Col span={24}>
        <LoginFormComponent />
      </Col>
    </Row>
  );
}

export default LoginForm;
