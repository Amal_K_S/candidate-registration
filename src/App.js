import logo from "./logo.svg";
import "./App.css";
import LoginForm from "./pages/LoginForm/LoginForm";

function App() {
  return <LoginForm />;
}

export default App;
